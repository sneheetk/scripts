#! /usr/bin/python3.6

import pathlib
import re

# todo - typing, linting, and all that.

def get_next_cpp_file(root_path : str) -> pathlib.Path:
    root_dir = pathlib.Path(root_path).expanduser()
    for ext in ('h', 'cpp', 'ipp'):
        for item in root_dir.glob('**/*.{}'.format(ext)):
            yield item


def get_exception_name(line):
    pattern = '(REGISTER_EXCEPTION_CODE)(\()(\w*)'
    result = re.search(pattern, line)
    if result:
        return result.group(3)


def get_regisered_exceptions(dir):
    files_and_exceptions = set()

    for cpp_file in get_next_cpp_file(dir):
        with cpp_file.open('r') as f:
            for line in f:
                exception_name = get_exception_name(line)
                if exception_name:
                    files_and_exceptions.add((str(cpp_file.absolute()), exception_name))

    return files_and_exceptions


def is_exception_in_use(exception_name, lines):
    search = re.compile(r'\b({0})\b'.format(exception_name)).search

    for line in lines:
        # involve regex only when prelim checks pass for efficiency
        if 'REGISTER_EXCEPTION_CODE' not in line and exception_name in line and search(line):
            return True
    return False


def get_unused_exceptions(dir, files_and_exceptions):
    exceptions_in_use = set()

    for cpp_file in get_next_cpp_file(dir):
        with cpp_file.open('r') as f:
            lines = f.readlines()
            for file_and_exception in files_and_exceptions:
                if file_and_exception not in exceptions_in_use and is_exception_in_use(file_and_exception[1], lines):
                    exceptions_in_use.add(file_and_exception)

    return files_and_exceptions.difference(exceptions_in_use)


def remove_unused_exceptions(files_and_unused_exceptions):
    for file_and_exception in files_and_unused_exceptions:
        file_name, unused_exception = file_and_exception
        lines = []
        with open(file_name, 'r') as f:
            lines = f.readlines()
        unused_exception_str = 'REGISTER_EXCEPTION_CODE(' + unused_exception + ')'
        with open(file_name, 'w') as f:
            for line in lines:
                if unused_exception_str not in line:
                    f.write(line)


if __name__ == "__main__":
    files_and_exceptions = get_regisered_exceptions('/data/sandbox/element')
    print(len(files_and_exceptions))
    #print(files_and_exceptions)

    files_and_unused_exceptions = get_unused_exceptions('/data/sandbox/element', files_and_exceptions)
    print(len(files_and_unused_exceptions))
    #print(files_and_unused_exceptions)

    remove_unused_exceptions(files_and_unused_exceptions)
    # todo - now for each IP, need to go through each file's each line and see if match exists (excluding REGISTER_INJECTION_POINT). fix xPointID

